window.addEventListener("load", ()=>{
	setInterval(function(){
		var d=new Date();
		var hora=d.getHours();
		var minutos=d.getMinutes();
		var segundos=d.getSeconds();
		segundos=segundos.toString();
		minutos=minutos.toString();
		hora=hora.toString();
		if(segundos<10){
			segundos="0"+segundos;
		}
		if(minutos<10){
			minutos="0"+minutos;
		}
		if(hora<10){
			hora="0"+hora;
		}
		var digitos=[];
		digitos[0]=hora.charAt(0);
		digitos[1]=hora.charAt(1);
		digitos[2]=minutos.charAt(0);
		digitos[3]=minutos.charAt(1);
		digitos[4]=segundos.charAt(0);
		digitos[5]=segundos.charAt(1);
		
		var imagenes=document.querySelectorAll(".digitos");
		for(var i=0;i<imagenes.length;i++){
			imagenes[i].src="imgs/digito"+digitos[i]+".jpg";
		}
	}, 300);
});